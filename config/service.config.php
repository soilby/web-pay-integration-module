<?php

return array(
    'service_manager' => array(
        'alias' => [

        ],
        'invokables' => [
        ],
        'factories' => [
            'TalakaWebpayTransactionBuilder' => 'Talaka\WebPayIntegration\Service\Factory\TransactionBuilderFactory',
            'WebPayPaymentApi' => 'Talaka\WebPayIntegration\Api\Factory\PaymentFactory'
        ]
    ),
    'controllers' => array(
        'invokables' => array(
        ),
        'factories' => [

        ]
    ),
    'view_helpers' => array(
        'invokables' => array(
        ),
        'factories' => [
            'webPayIntegrationWidget' => 'Talaka\WebPayIntegration\View\Factory\WidgetFactory'
        ]
    )
);
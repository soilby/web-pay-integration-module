<?php

namespace Incubator;

return array(
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        )
    ), // doctrine

    'web_pay_integration' => [
        'user' => 'Talaka',
//        'pass_md5' => '01aaa39593b02b64aee0481c1b7bb8ec', //sandbox

        'pass_md5' => 'db1837b7cb7d8837a0b17f660eab4807',

        'secret_key' => 'ba86101a1dd2550fcdd158f16ecfe5eb',
        'sandbox_endpoint' => 'https://securesandbox.webpay.by/',
        'sandbox_billing_endpoint' => 'https://sandbox.webpay.by',
        'production_endpoint' => 'https://payment.webpay.by/services',
        'production_billing_endpoint' => 'https://billing.webpay.by',
        'sandbox' => false,

//        'storeId' => '139917840',//test
        'storeId' => '845076709',
        'storeName' => 'Talaka',
        'currencyCode' => 'BYN',
        'returnURL' => '/payment/callback/success/webpay',
        'cancelReturnUrl' => '/payment/callback/fail/webpay',
        'notifyUrl' => 'http://www.talaka.by/payment/notify/webpay',
        'service_number' => '432171'

    ]
);
<?php
namespace Talaka\WebPayIntegration\View;

use User\Entity\User;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;

/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 27.3.15
 * Time: 20.48
 */

class Widget extends AbstractHelper {

    protected $endpointURL;
    protected $storeId;
    protected $storeName;
    protected $currencyCode;

    protected $returnURL;
    protected $cancelReturnURL;
    protected $notifyUrl;


    protected $shippingName = '';
    protected $shippingPrice = 0;

    protected $items = [];
    protected $total = 0;

    protected $orderId;

    protected $secretKey;
    
    protected $sandbox = false;

    /**
     * @var string
     * номер услуги belqi  в ЕРИП
     * wsb_service_number
     */
    protected $serviceNumber;

    /**
     * @var integer
     * лицевой счет, в данном случае номер кошелька
     * wsb_service_account
     */
    protected $serviceAccount;

    /**
     * @var User
     */
    protected $customer;

    public function __construct($endpointURL, $storeId, $storeName,
                                $currencyCode, $returnURL, $cancelReturnURL, $sandbox)    {
        $this->endpointURL = $endpointURL;
        $this->storeId = $storeId;
        $this->storeName = $storeName;
        $this->currencyCode = $currencyCode;
        $this->returnURL = $returnURL;
        $this->cancelReturnURL = $cancelReturnURL;
        $this->sandbox = $sandbox;
    }

    public function setCustomer($customer)  {
        $this->customer = $customer;
    }

    public function setItems($items)    {
        $this->items = $items;
        $this->recalculateTotal();
    }

    public function recalculateTotal()  {

        $total = 0;
        foreach ($this->items as $item)   {
            $total += $item['price'] * $item['quantity'];
        }

        $this->total = $total;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @param mixed $secretKey
     */
    public function setSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;
    }

    /**
     * @param int $serviceAccount
     */
    public function setServiceAccount($serviceAccount)
    {
        $this->serviceAccount = $serviceAccount;
    }

    /**
     * @param string $serviceNumber
     */
    public function setServiceNumber($serviceNumber)
    {
        $this->serviceNumber = $serviceNumber;
    }
    
    


    public function __invoke($storeName = null)  {
        $this->setItems([]);
        
        if ($storeName) {
            $this->storeName = $storeName;
        }

        return $this;
    }

    public function addItem($itemName, $itemPrice)  {
        $this->items[] = [
            'name' => $itemName,
            'quantity' => 1,
            'price' => $itemPrice
        ];

        $this->recalculateTotal();
    }
    
    




    public function __toString()    {

        $viewModel = new ViewModel([
            'endpointURL' => $this->endpointURL,
            'storeId' => $this->storeId,
            'storeName' => $this->storeName,
            'currencyCode' => $this->currencyCode,
            'returnUrl' => $this->returnURL,
            'cancelReturnUrl' => $this->cancelReturnURL,
            'notifyUrl' => $this->notifyUrl,

            'shippingName' => $this->shippingName,
            'shippingPrice' => $this->shippingPrice,


            'currencyId' => $this->currencyCode,
            'items' => $this->items,
            'total' => $this->total,

            'tax' => 0,
            'isTest' => $this->sandbox,

            'orderId' => $this->orderId,

            'serviceAccount' => $this->serviceAccount,
            'serviceNumber' => $this->serviceNumber
            
        ]);

        if ($this->customer)    {
            $viewModel->setVariable('email', $this->customer->getEmail());
            $viewModel->setVariable('phone', $this->customer->getPhone());
        }

        $viewModel->setVariable('seed', $this->getRandomSeed());


        if ($this->secretKey) {
            $variables = ['seed', 'storeId', 'orderId', 'isTest', 'currencyCode', 'total', 'serviceNumber', 'serviceAccount'];
            //        $secretKey = 'ba86101a1dd2550fcdd158f16ecfe5eb';
            $secretKey = $this->secretKey;

            $signatureSource = '';
            foreach ($variables as $variable) {
                $signatureSource .= $viewModel->getVariable($variable);
            }
            $signatureSource .= $secretKey;


            $signature = sha1($signatureSource);

            $viewModel->setVariable('signature', $signature);
        }

        $viewModel->setTemplate('web-pay-integration-module/widget');

        return $this->view->render($viewModel);
    }

    protected function getRandomSeed($length = 12)  {
//        return time();
        return substr(md5(time()), 0, $length);

    }

    /**
     * @param mixed $notifyUrl
     */
    public function setNotifyUrl($notifyUrl)
    {
        $this->notifyUrl = $notifyUrl;
    }

    /**
     * @return mixed
     */
    public function getStoreName()
    {
        return $this->storeName;
    }

    /**
     * @param mixed $storeName
     */
    public function setStoreName($storeName)
    {
        $this->storeName = $storeName;
    }
    
    
    

}
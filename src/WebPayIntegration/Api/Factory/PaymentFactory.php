<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 16.4.15
 * Time: 23.25
 */

namespace Talaka\WebPayIntegration\Api\Factory;

use Talaka\WebPayIntegration\Api\PaymentApi;
use Talaka\WebPayIntegration\View\Widget;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class PaymentFactory implements FactoryInterface {

    protected $serverUrl;

    public function createService(ServiceLocatorInterface $serviceLocator)  {

        $config = $serviceLocator->get('config');
        if (!array_key_exists('web_pay_integration', $config))  {
            throw new \Exception('Web Pay Integration module should be configure under web_pay_integration key');
        }

        $moduleConfig = $config['web_pay_integration'];

        $user = $moduleConfig['user'];
        $passMd5 = $moduleConfig['pass_md5'];

        $sandbox = array_key_exists('sandbox', $moduleConfig) ? $moduleConfig['sandbox'] : false;

        if ($sandbox)   {
            if (!array_key_exists('sandbox_billing_endpoint', $moduleConfig))  {
                throw new \Exception("Sandbox billing endpoint didn't set. Please specify under web_pay_integration/sandbox_endpoint key.");
            }
            $endpointURL = $moduleConfig['sandbox_billing_endpoint'];
        }
        else    {
            if (!array_key_exists('production_billing_endpoint', $moduleConfig))  {
                throw new \Exception("Production billing endpoint didn't set. Please specify under web_pay_integration/production_endpoint key.");
            }
            $endpointURL = $moduleConfig['production_billing_endpoint'];
        }

        $service = new PaymentApi($user, $passMd5, $endpointURL);

        return $service;
    }

} 
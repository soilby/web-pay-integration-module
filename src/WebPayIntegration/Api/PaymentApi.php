<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 16.4.15
 * Time: 23.20
 */

namespace Talaka\WebPayIntegration\Api;


use Talaka\Payment\Entity\Notification;
use Zend\Http\Client;

class PaymentApi {

    protected $user;
    protected $passMd5;
    protected $apiEndpoint;


    public function __construct($user, $passMd5, $apiEndpoint)   {
        $this->user = $user;
        $this->passMd5 = $passMd5;
        $this->apiEndpoint = $apiEndpoint;

    }

    public function getTransaction($transactionId)   {
        $writer = new \XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0','ISO-8859-1');
        $writer->setIndent(4);
        $writer->startElement('wsb_api_request');
            $writer->startElement('command');
                $writer->text('get_transaction');
            $writer->endElement();
            $writer->startElement('authorization');
                $writer->startElement('username');
                    $writer->text($this->user);
                $writer->endElement();
                $writer->startElement('password');
                    $writer->text($this->passMd5);
                $writer->endElement();
            $writer->endElement();

            $writer->startElement('fields');
                $writer->startElement('transaction_id');
                    $writer->text($transactionId);
                $writer->endElement();
            $writer->endElement();
        $writer->endElement();

        $writer->endDocument();
        $xml = $writer->outputMemory(true);

        $request = '*API=&API_XML_REQUEST=' . urlencode($xml);

        $curl = curl_init($this->apiEndpoint);
        curl_setopt ($curl, CURLOPT_HEADER, 0);
        curl_setopt ($curl, CURLOPT_POST, 1);
        curl_setopt ($curl, CURLOPT_POSTFIELDS, $request);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $response = curl_exec ($curl);
        curl_close ($curl);

        if (!$response)  {
            throw new \Exception("No response");
        }
        
file_put_contents('/tmp/payment_req', $request);
file_put_contents('/tmp/payment_resp', $response);

        $dom = new \DOMDocument();
        $result = $dom->loadXML($response);
        if ($result === false)  {
            throw new \Exception('Response malformed');
        }

        if ($dom->getElementsByTagName('wsb_api_response')->length === 0)   {
            throw new \Exception('Response malformed');
        }

        $status = $dom->getElementsByTagName('status')->item(0)->textContent;
        $fields = $this->getFields($dom->getElementsByTagName('fields')->item(0));

        return [
            'status' => $status,
            'fields' => $fields,
            'origin' => $response
        ];

    }

    protected function getFields($domNode)  {
        $notification = new Notification();

        $fields = [];
        foreach ($domNode->childNodes as $field) {
            if ($field->nodeType === XML_ELEMENT_NODE) {
                $property = $field->nodeName;
                $fields[$property] = $field->textContent;
            }
        }
        $notification->origin = $fields;

        $notification->currency = $fields['currency_id'];
        $notification->amount = $fields['amount'];
        $notification->foreignOrderId = $fields['order_id'];
        $notification->orderId = $fields['order_num'];
        $notification->foreignTransactionId = $fields['transaction_id'];
        $notification->signature = $fields['wsb_signature'];

        $timeString = date('c', $fields['batch_timestamp']);
        $notification->date = new \DateTime($timeString);

        $notification->transactionType = $fields['payment_type'];

        return $notification;
    }
} 
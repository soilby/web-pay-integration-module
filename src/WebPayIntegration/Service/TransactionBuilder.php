<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 17.4.15
 * Time: 2.32
 */

namespace Talaka\WebPayIntegration\Service;


use Talaka\Payment\Entity\Notification;
use Talaka\Payment\Entity\OrderAbstract;
use Talaka\Payment\Entity\Transaction;
use Talaka\Payment\Service\OrderService;
use Talaka\Payment\Transaction\BuilderInterface;
use Talaka\WebPayIntegration\Api\PaymentApi;
use Zend\Http\Request;

class TransactionBuilder implements BuilderInterface {

    const TRANSACTION_TYPE_COMPLETED = 1;
    const TRANSACTION_TYPE_DECLINED = 2;
    const TRANSACTION_TYPE_PENDING = 3;
    const TRANSACTION_TYPE_AUTHORIZED = 4;
    const TRANSACTION_TYPE_REFUNDED = 5;
    const TRANSACTION_TYPE_SYSTEM = 6;
    const TRANSACTION_TYPE_VOIDED = 7;


    /**
     * @var PaymentApi
     */
    protected $paymentApi;

    protected $secretKey;

    public function __construct($paymentApi)   {
        $this->paymentApi = $paymentApi;
    }

    /**
     * @param mixed $secretKey
     */
    public function setSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;
    }


    /**
     * @param Request $request
     *
     * @return Notification
     * @throws \Exception
     */
    public function handleNotification(Request $request)    {
        $content = $request->getContent();

        $body = urldecode($content);
        $fields = [];
        parse_str($body, $fields);

        if (!$fields) {
            throw new \Exception("Cannot parse notification");
        }

        $notification = $this->parseNotification($fields);

        return $notification;

    }


    /**
     * @param $fields
     *
     * @return Notification
     */
    public function parseNotification($fields) {
        $notification = new Notification();

        $notification->currency = $fields['currency_id'];
        $notification->amount = $fields['amount'];
        $notification->foreignOrderId = $fields['order_id'];
        $notification->orderId = $fields['site_order_id'];
        $notification->foreignTransactionId = $fields['transaction_id'];

        $timeString = date('c', $fields['batch_timestamp']);
        $notification->date = new \DateTime($timeString);

        $notification->transactionType = $fields['payment_type'];
        $notification->signature = $fields['wsb_signature'];

        return $notification;
    }

    protected function checkSum(Notification $parsedFields) {

    }

    /**
     * @param Request $request
     *
     * @return Notification
     * @throws \Exception
     */
    public function getTransaction(Request $request)    {
//        $request->getQuery('wsb_order_num')
        $foreignTransactionId = $request->getQuery('wsb_tid');

        if ($foreignTransactionId) {
            $notification = $this->obtainTransactionById($foreignTransactionId);
        }
        else    {
            //get empty one

            $orderId = $request->getQuery('wsb_order_num');

            $notification = new Notification();
            $notification->orderId = $orderId;
            $notification->status = self::TRANSACTION_TYPE_DECLINED;

        }

        return $notification;
    }

    public function obtainTransactionById($foreignTransactionId)   {
        $response = $this->paymentApi->getTransaction($foreignTransactionId);

        $requestStatus = $response['status'];
        if ($requestStatus !== 'success')   {
            throw new \Exception('Unable to obtain transaction');
        }

        return $response['fields'];
    }

    public function updateTransaction(Transaction $transaction, Notification $parsedFields = null)    {


        switch ($parsedFields->transactionType)    {
            case self::TRANSACTION_TYPE_AUTHORIZED:
            case self::TRANSACTION_TYPE_COMPLETED:
                $status = OrderAbstract::STATUS_CONFIRMED;

                break;

            case self::TRANSACTION_TYPE_DECLINED:
                $status = OrderAbstract::STATUS_CANCELED;

                break;

            case self::TRANSACTION_TYPE_PENDING:
                $status = OrderAbstract::STATUS_PLACED;

                break;

            case self::TRANSACTION_TYPE_VOIDED:
            case self::TRANSACTION_TYPE_REFUNDED:
                $status = OrderAbstract::STATUS_REFUNDED;

                break;

            default:
                $status = null;

        }


        $transaction->setStatus($status);


        return $transaction;
    }

}
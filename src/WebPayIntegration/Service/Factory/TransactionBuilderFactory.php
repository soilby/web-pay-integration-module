<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 20.4.15
 * Time: 4.04
 */

namespace Talaka\WebPayIntegration\Service\Factory;


use Talaka\WebPayIntegration\Service\TransactionBuilder;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TransactionBuilderFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $api = $serviceLocator->get('WebPayPaymentApi');

        $service = new TransactionBuilder($api);

        return $service;
    }
}